Resurrection Remix for i9100 - How to Build?
-------------

![rr logo](https://gitlab.com/getawaycar/isolatedworld/RR/rr-manifests/raw/master/rr.png)

```
1. mkdir -p RR

2. cd RR

3. Initialize your local repository using the Resurrection Remix trees, use a command
  repo init -u https://github.com/ResurrectionRemix/platform_manifest.git -b oreo

4. Clone my repo:
  git clone https://gitlab.com/getawaycar/isolatedworld/RR/rr-manifests.git -b master .repo/local_manifests

5. Sync the repo:
  repo sync --no-tags --no-clone-bundle --force-sync -c

6. To build:
  . build/envsetup.sh
  brunch i9100
```


Credits
-------
* [**rINanDO**](https://github.com/rINanDO)
* [**Resurrection Remix**](https://github.com/ResurrectionRemix)


